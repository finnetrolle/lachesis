package DrawingHints;

import Issues.BaseIssue;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Класс представляет подсказку для отрисовки комнаты
 * @author finnetrolle
 */
public class RoomDrawingHint extends BaseDrawingHint {

    /**
     * Конструктор
     */
    public RoomDrawingHint() {
        super();
    }
    
    /** Рекомендуемый конструктор
     * 
     * @param x - координата x верхнего левого угла относительно родителя
     * @param y - координата y верхнего левого угла относительно родителя
     * @param width - ширина
     * @param height - высота
     */
    public RoomDrawingHint(int x, int y, int width, int height) {
        super();
        super.setX(x);
        super.setY(y);
        super.setWidth(width);
        super.setHeight(height);
    }

    /**
     * Метод устанавливает фоновый цвет
     * @param backgroundColor - фоновый цвет
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Метод устанавливает цвет рамки
     * @param borderColor - цвет рамки
     */
    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * Перегрузка метода отрисовки
     * @param g - графический контекст
     * @param issueLevel - уровень проблемы
     */
    @Override
    public void draw(Graphics g, BaseIssue.IssueLevel issueLevel) {
        g.setColor(backgroundColor);
        g.fillRoundRect(super.getX(), super.getY(), 
                super.getWidth(), super.getHeight(), 5, 5);
        g.setColor(borderColor);
        g.drawRoundRect(super.getX(), super.getY(), 
                super.getWidth(), super.getHeight(), 5, 5);
    }

    /**
     * цвет рамки
     */
    private Color borderColor = Color.BLACK;
    
    /**
     * Цвет фона
     */
    private Color backgroundColor = Color.LIGHT_GRAY;
}
