package DrawingHints;

import Issues.BaseIssue;
import java.awt.Image;
import java.awt.Graphics;

/**
 * Класс, описывающий подсказку, рисующую картинки
 * @author finnetrolle
 */
public class ImageDrawingHint extends BaseDrawingHint {

    /**
     * Базовый конструктор.
     */
    public ImageDrawingHint() {
        super();
    }

    /** Рекомендуемый конструктор
     * 
     * @param defaultImage - ссылка на картинку по умолчанию
     * @param notificationImage - ссылка на картинку для оповещения
     * @param warningImage - ссылка на картинку предупреждения
     * @param errorImage - ссылка на картинку ошибки
     * @param criticalImage - ссылка на картинку критической ошибки
     */
    public ImageDrawingHint(Image defaultImage, Image notificationImage, Image warningImage, Image errorImage, Image criticalImage) {
        super();
        this.defaultImage = defaultImage;
        this.notificationImage = notificationImage;
        this.warningImage = warningImage;
        this.errorImage = errorImage;
        this.criticalImage = criticalImage;
        this.resizeByImage(defaultImage, true, false);
    }

    /**
     * Метод рисует подсказку
     * @param g - графический контекст
     * @param issueLevel - максимальный уровень проблемы
     */
    @Override
    public void draw(Graphics g, BaseIssue.IssueLevel issueLevel) {
        Image toDraw;
        switch (issueLevel){
            case I_DEADLY: toDraw = criticalImage; break;
            case I_CRITICAL: toDraw = criticalImage; break;
            case I_MAJOR: toDraw = errorImage; break;
            case I_MINOR: toDraw = errorImage; break;
            case I_WARNING: toDraw = warningImage; break;
            case I_NOTICE: toDraw = notificationImage; break;
            default : toDraw = defaultImage;
        }
        g.drawImage(toDraw, super.getX(), super.getY(), null);
    }

    /**
     * метод устанавливает картинку для одноименого состояния
     * @param criticalImage - ссылка на картинку
     */
    public void setCriticalImage(Image criticalImage) {
        this.criticalImage = criticalImage;
        resizeByImage(criticalImage, true, false);
    }

    /**
     * метод устанавливает картинку для одноименого состояния
     * @param defaultImage - ссылка на картинку
     */
    public void setDefaultImage(Image defaultImage) {
        this.defaultImage = defaultImage;
        resizeByImage(defaultImage, true, false);
    }

    /**
     * метод устанавливает картинку для одноименого состояния
     * @param errorImage - ссылка на картинку
     */
    public void setErrorImage(Image errorImage) {
        this.errorImage = errorImage;
        resizeByImage(errorImage, true, false);
    }

    /**
     * метод устанавливает картинку для одноименого состояния
     * @param notificationImage  - ссылка на картинку
     */
    public void setNotificationImage(Image notificationImage) {
        this.notificationImage = notificationImage;
        resizeByImage(notificationImage, true, false);
    }

    /**
     * метод устанавливает картинку для одноименого состояния
     * @param warningImage - ссылка на картинку
     */
    public void setWarningImage(Image warningImage) {
        this.warningImage = warningImage;
        resizeByImage(warningImage, true, false);
    }
    
    /**
     * Метод позволяет автоматически подогнать ширину и высоту по параметрам
     * картинки, передаваемой в параметрах.
     * @param image - ссылка на картинку
     * @param allowIncrease - флаг разрешения увеличения 
     * @param allowDecrease - флаг разрешения уменьшения
     */
    private void resizeByImage(Image image, 
            boolean allowIncrease, boolean allowDecrease) {
        if (allowIncrease == true) {
            if (image.getWidth(null) > super.getWidth()) {
                super.setWidth(image.getWidth(null));
            }
            if (image.getHeight(null) > super.getHeight()) {
                super.setHeight(image.getHeight(null));
            }
        }
        if (allowDecrease == true) {
            if (image.getWidth(null) < super.getWidth()) {
                super.setWidth(image.getWidth(null));
            }
            if (image.getHeight(null) > super.getHeight()) {
                super.setHeight(image.getHeight(null));
            }
        }
    }
    
    /**
     * Ссылки на картинки
     */
    private Image defaultImage;         // состояние по умолчанию
    private Image notificationImage;    // есть нотификейшн
    private Image warningImage;         // есть предупреждение
    private Image errorImage;           // есть ошибка
    private Image criticalImage;        // критическое состояние
    
}
