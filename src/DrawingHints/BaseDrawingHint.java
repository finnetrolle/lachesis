package DrawingHints;

import Issues.BaseIssue;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;

/**
 * Класс описывает базовый элемент графической подсказки, которая хранит в себе
 * описание рисования элемента и метод draw
 * @author finnetrolle
 */
public class BaseDrawingHint {
    
    /** Конструктор по умолчанию
     * 
     */
    public BaseDrawingHint() {
    }

    /**
     * Метод возвращает идентификатор элемента, для которого хранит подсказку
     * @return id элемента
     */
    public int getElementId() {
        return elementId;
    }

    /**
     * Метод возвращает высоту рисуемой подсказки
     * @return высота элемента
     */
    public int getHeight() {
        return height;
    }

    /**
     * Метод возвращает ширину рисуемой подсказки
     * @return ширина элемента
     */
    public int getWidth() {
        return width;
    }

    /**
     * Метод возвращает координату X верхнего левого угла, относительно родителя
     * @return X
     */
    public int getX() {
        return x;
    }

    /**
     * Метод возвращает координату Y верхнего левого угла, относительно родителя
     * @return Y
     */
    public int getY() {
        return y;
    }

    /**
     * Метод устанавливает идентификатор элемента, 
     * для которого хранится подсказка
     * @param elementId - идентификатор элемента
     */
    public void setElementId(int elementId) {
        this.elementId = elementId;
    }

    /**
     * Метод устанавливает высоту подсказки
     * @param height - высота
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Метод устанавливает ширину подсказки
     * @param width - ширина
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Метод устанавливает координату X верхнего левого угла, 
     * относительно родителя
     * @param x - X
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Метод устанавливает координату Y верхнего левого угла,
     * относительно родителя
     * @param y - Y
     */
    public void setY(int y) {
        this.y = y;
    }
    
    /** 
     * Метод отрисовывает графическую подсказку
     * @param g - графикс, который управляет отрисовкой
     * @param issueLevel - максимальный уровень проблемы элемента
     */
    public void draw(Graphics g, BaseIssue.IssueLevel issueLevel) {
        
    }
    
    /**
     * Координата X верхнего левого угла подсказки относительно родителя
     */
    protected int x = 0;
    
    /**
     * Координата Y верхнего левого угла подсказки относительно родителя
     */
    protected int y = 0;
    
    /** 
     * Ширина подсказки
     */
    protected int width = 0;
    
    /**
     * Высота подсказки
     */
    protected int height = 0;
    
    /**
     * Уникальный идентификатор элемента, для которого создана подсказка
     */
    protected int elementId = 0;
    
    protected static Color aliveColor = Color.cyan;
    protected static Color issuedColor = Color.yellow;
    protected static Color failureColor = Color.red;
    protected static Color unknownColor = Color.gray;
    
    protected static Image iNone = null;
    protected static Image iNotice = null;
    protected static Image iWarning = null;
    protected static Image iMinor = null;
    protected static Image iMajor = null;
    protected static Image iCritical = null;
    protected static Image iDeadly = null;
    
    protected static Image iActive = null;
    protected static Image iReserved = null;
    protected static Image iPlayer = null;
    protected static Image iAutonomy = null;
    protected static Image iOffline = null;

    public static void setiActive(Image iActive) {
        BaseDrawingHint.iActive = iActive;
    }

    public static void setiAutonomy(Image iAutonomy) {
        BaseDrawingHint.iAutonomy = iAutonomy;
    }

    public static void setiCritical(Image iCritical) {
        BaseDrawingHint.iCritical = iCritical;
    }

    public static void setiDeadly(Image iDeadly) {
        BaseDrawingHint.iDeadly = iDeadly;
    }

    public static void setiMajor(Image iMajor) {
        BaseDrawingHint.iMajor = iMajor;
    }

    public static void setiMinor(Image iMinor) {
        BaseDrawingHint.iMinor = iMinor;
    }

    public static void setiNone(Image iNone) {
        BaseDrawingHint.iNone = iNone;
    }

    public static void setiNotice(Image iNotice) {
        BaseDrawingHint.iNotice = iNotice;
    }

    public static void setiOffline(Image iOffline) {
        BaseDrawingHint.iOffline = iOffline;
    }

    public static void setiPlayer(Image iPlayer) {
        BaseDrawingHint.iPlayer = iPlayer;
    }

    public static void setiReserved(Image iReserved) {
        BaseDrawingHint.iReserved = iReserved;
    }

    public static void setiWarning(Image iWarning) {
        BaseDrawingHint.iWarning = iWarning;
    }
    
    protected static void drawDefaultIssueImage(Graphics g, 
            BaseIssue.IssueLevel issueLevel,
            int x, int y, int width, int height) {
        Image img = null;
        switch (issueLevel) {
            case I_INVALIDISSUE: img = iNone; break;
            case I_NOTICE: img = iNotice; break;
            case I_WARNING: img = iWarning; break;
            case I_MINOR: img = iMinor; break;
            case I_MAJOR: img = iMajor; break;
            case I_CRITICAL: img = iCritical; break;
            case I_DEADLY: img = iDeadly; break;
            default: img = iNone;
        }
        int rx = x + width - 20;
        g.drawImage(img, rx, y, null);
    }
    
    protected static void drawDefaultStatusImage(Graphics g,
            int status, 
            int x, int y, int width, int height) {
        Image img = null;
        switch (status) {
            case 0: img = iOffline; break;
            case 1: img = iActive; break;
            case 2: img = iPlayer; break;
            case 3: img = iReserved; break;
            case 4: img = iAutonomy; break;
            default: img = iActive;
        }
        g.drawImage(img, x, y, null);
    }
    
    protected static void drawDefaultHeaderText(Graphics g,
            String header,
            int x, int y, int width, int height) {
        g.drawString(header, x + 20, y);
    }
    
    protected static void drawDefaultFrame(Graphics g, Color foreColor, 
            Color backColor, Color headerColor, 
            int x, int y, int width, int height) {
        g.setColor(backColor);
        g.fillRoundRect(x, y, width, height, 10, 10);
        g.setColor(headerColor);
        g.fillRoundRect(x, y, 20, 20, 10, 10);
        g.setColor(foreColor);
        g.drawRoundRect(x, y, width, height, 10, 10);
    }
    
    
    
    
}
