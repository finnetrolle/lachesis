/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DrawingHints;

import Issues.BaseIssue;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author finnetrolle
 */
public class WorkplaceDrawingHint extends BaseDrawingHint {

    public WorkplaceDrawingHint() {
        super();
    }

    public WorkplaceDrawingHint(int x, int y, int width, int height) {
        super();
        super.setX(x);
        super.setY(y);
        super.setWidth(width);
        super.setHeight(height);
        this.workplaceName = "name!";
    }

    @Override
    public void draw(Graphics g, BaseIssue.IssueLevel issueLevel) {
        BaseDrawingHint.drawDefaultFrame(g, 
                Color.black, Color.white, aliveColor, 
                x, y, width, height);
        BaseDrawingHint.drawDefaultHeaderText(g, this.workplaceName, 
                x, y, width, height);
        BaseDrawingHint.drawDefaultIssueImage(g, issueLevel, 
                x, y, width, height);
        BaseDrawingHint.drawDefaultStatusImage(g, 1, 
                x, y, width, height);
    }

    public String getWorkplaceName() {
        return workplaceName;
    }

    public void setWorkplaceName(String workplaceName) {
        this.workplaceName = workplaceName;
    }
    
    
    
    private String workplaceName;
    
    
}
