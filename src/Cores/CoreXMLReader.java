package Cores;

import DrawingHints.ImageDrawingHint;
import DrawingHints.RoomDrawingHint;
import DrawingHints.BaseDrawingHint;
import DrawingHints.WorkplaceDrawingHint;
import Elements.HDDElement;
import Elements.CPUElement;
import Elements.RoomElement;
import Elements.DesktopElement;
import Elements.BaseElement;
import Elements.WorkplaceElement;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Статический класс, реализующий методы для загрузки из XML файлов
 * различных данных
 * @author finnetrolle
 */
public class CoreXMLReader {
    
    /**
     * Статический метод для загрузки ядра подсказок
     * @param dhCore - ссылка на ядро подсказок
     * @param fileName - имя файла с подсказками
     */
    static public void fillDrawingHintCore(DrawingHintCore dhCore, String fileName) {
        File file = new File("defaultpc.png");
        Image img;
        try {
            img = ImageIO.read(file);
        } catch (java.io.IOException e) {
            e.printStackTrace();
            img = null;
        }
        
        try {
            // open
            File fXmlFile = new File(fileName);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(fXmlFile);
            
            // normalize
            doc.getDocumentElement().normalize();
            
            // going in
            NodeList nlist = doc.getDocumentElement().getChildNodes();
            
            for (int i = 0; i < nlist.getLength(); ++i) {
                if (nlist.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element)nlist.item(i);
                    BaseDrawingHint hint;
                    switch (nlist.item(i).getNodeName()) {
                    case "workplace": hint = new WorkplaceDrawingHint(
                            Integer.valueOf(el.getAttribute("x")),
                            Integer.valueOf(el.getAttribute("y")),
                            Integer.valueOf(el.getAttribute("width")),
                            Integer.valueOf(el.getAttribute("height"))); break;
                    case "room": hint = new RoomDrawingHint(
                            Integer.valueOf(el.getAttribute("x")),
                            Integer.valueOf(el.getAttribute("y")),
                            Integer.valueOf(el.getAttribute("width")),
                            Integer.valueOf(el.getAttribute("height"))); break;
                    case "desktop": hint = 
                            new ImageDrawingHint(img, img, 
                                    img, img, img); break;
                    default: hint = new BaseDrawingHint();
                    }
                    hint.setElementId(Integer.valueOf(el.getAttribute("id")));
                    hint.setX(Integer.valueOf(el.getAttribute("x")));
                    hint.setY(Integer.valueOf(el.getAttribute("y")));
                    dhCore.addDrawingHint(hint.getElementId(), hint);
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Статический метод для загрузки ядра элементов из XML файла
     * @param filename - имя файла
     * @return ссылка на созданное дерево
     */
    public static BaseElement fillTreeWithXML(String filename){
        BaseElement result = new BaseElement();
        
        try {
            // open xml file
            File fXmlFile = new File("DefaultTree.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            
            // normalize
            doc.getDocumentElement().normalize();
            
            // going in 
            NodeList nlist = doc.getDocumentElement().getChildNodes();
            formTree(result, nlist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Приватный метод, позволяющий рекурсивно прочитать XML файл с элементами
     * @param root - ссылка на текущий элемент
     * @param nlist - список нод, из которых необходимо читать в элемент
     */
    private static void formTree(BaseElement root, NodeList nlist){
        for (int i = 0; i < nlist.getLength(); i++){
            Node node = nlist.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element el = (Element)node;
                String key = node.getNodeName();
                int id = Integer.valueOf(el.getAttribute("id"));
                BaseElement item = null;
                
                if (key.equals("room")){
                    item = new RoomElement(id, root);
                }
                if (key.equals("desktop")){
                    item = new DesktopElement(id, root);
                }
                if (key.equals("hdd")){
                    item = new HDDElement(id, root);
                }
                if (key.equals("cpu")){
                    item = new CPUElement(id, root);
                }
                if (key.equals("workplace")){
                    item = new WorkplaceElement("Place", id, root);
                }
                
                if (item != null){
                    if (node.hasChildNodes()){
                        NodeList list = node.getChildNodes();
                        formTree(item, list);
                    }
                }
                
            }
        }
    }
    
}
