package Cores;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import Issues.BaseIssue;

/**
 * Класс реализует хранение и работу с проблемами. 
 * Данный класс не умеет смотреть вглубь по дереву элементов и служит лишь
 * для создания связей элемент - проблема и хранения общего списка проблем
 * @author finnetrolle
 */
public class IssueCore {

    /** 
     * Конструктор
     */
    public IssueCore() {
        issueList = new ArrayList<>();
        issueHash = new HashMap<>();
        issueIdHash = new HashMap<>();
    }
    
    /**
     * Метод добавляет проблему и регистрирует ее во всех контейнерах
     * @param issue - ссылка на проблему
     */
    public void addIssue(BaseIssue issue) {
        if (issue == null) {
            return;
        }
        issueList.add(issue);
        issueIdHash.put(issue.getId(), issue);
        // search for existing list in hash
        ArrayList<BaseIssue> list;
        if (issueHash.containsKey(issue.getOwnerId()) == false) {
            // we have no hash for this element. create
            list = new ArrayList<>();
        }
        else {
            // just take existing element
            list = issueHash.get(issue.getOwnerId());
        }
        list.add(issue);
    }
    
    /**
     * Метод удаляет проблему из всех контейнеров
     * @param issue - ссылка на проблему
     */
    public void removeIssue(BaseIssue issue) {
        issueList.remove(issue);
        issueIdHash.remove(issue.getId());
        ArrayList<BaseIssue> list = issueHash.get(issue.getOwnerId());
        if (list != null) {
            list.remove(issue);
            // clear list if no issues alive for element
            if (list.isEmpty()) {
                issueHash.remove(issue.getOwnerId());
            }
        }
    }
    
    /**
     * Метод удаляет проблему из всех контейнеров
     * @param issueId - идентификатор проблемы
     */
    public void removeIssue(int issueId) {
        if (issueIdHash.containsKey(issueId)) {
            BaseIssue issue = issueIdHash.get(issueId);
            removeIssue(issue);
        }
    }

    /**
     * Метод удаляет все проблемы, связанные с заданным элементом
     * @param elementId - идентификатор элемента
     * @return количество удаленных проблем
     */
    public int removeIssuesForElement(int elementId) {
        int deleted = 0;
        if (issueHash.containsKey(elementId)) {
            ArrayList<Integer> list = new ArrayList<>();
            ArrayList<BaseIssue> rlist = issueHash.get(elementId);
            for (int i = 0; i < rlist.size(); ++i) {
                list.add(rlist.get(i).getId());
            }
            for (int i = 0; i < list.size(); ++i) {
                this.removeIssue(list.get(i));
                deleted++;
            }
        }
        return deleted;
    }
    
    /**
     * Метод возвращает все проблемы, связанные с элементом
     * @param elementId - идентификатор элемента
     * @return ArrayList<BaseIssue> список проблем
     */
    public ArrayList<BaseIssue> getIssuesForElement(int elementId) {
        if (issueHash.containsKey(elementId)) {
            return issueHash.get(elementId);
        }
        else {
            return null;
        }
    }
    
    /**
     * Метод возвращает все проблемы, зарегестрированные в ядре
     * @return ArrayList<BaseIssue> список проблем
     */
    public ArrayList<BaseIssue> getIssues() {
        return issueList;
    }
    
    /**
     * Метод показывает есть ли у элемента проблемы
     * @param elementId - идентификатор элемента
     * @return true если есть проблемы, связанные с элементом, иначе false
     */
    public boolean isElementHaveIssues(int elementId) {
        return issueHash.containsKey(elementId);
    }
    
    /**
     * Метод возвращает максимальный уровень проблемы для элемента
     * @param elementId - идентификатор элемента
     * @return максимальный уровень проблемы
     */
    public BaseIssue.IssueLevel getMaximalIssueLevelForElement(int elementId) {
        if (issueHash.containsKey(elementId)) {
            ArrayList<BaseIssue> list = issueHash.get(elementId);
            return IssueCore.getMaximalIssueLevel(list);
        }
        return BaseIssue.IssueLevel.I_INVALIDISSUE;
    }
    
    /**
     * Метод возвращает максимальный уровень проблемы для всей системы
     * @return максимальный уровень проблемы
     */
    public BaseIssue.IssueLevel getMaximalIssueLevel() {
        return IssueCore.getMaximalIssueLevel(issueList);
    }
    
    /**
     * Метод возвращает проблему по ее идентификатору
     * @param issueId - идентификатор проблемы
     * @return ссылка на проблему
     */
    public BaseIssue getIssue(int issueId) {
        if (issueIdHash.containsKey(issueId)) {
            return issueIdHash.get(issueId);
        }
        return null;
    }
    
    /**
     * Метод возвращает максимальный уровень проблемы для проблем в списке
     * @param list список проблем
     * @return максимальный уровень проблемы
     */
    private static BaseIssue.IssueLevel getMaximalIssueLevel(
            ArrayList<BaseIssue> list) {
        BaseIssue.IssueLevel level = BaseIssue.IssueLevel.I_INVALIDISSUE;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i).getLevel().ordinal() > level.ordinal()) {
                level = list.get(i).getLevel();
            }
        }
        return level;
    }
    
    /**
     * Список проблем, куда входят все имеющиеся проблемы
     * Список не сортируется и все проблемы находятся в порядке поступления
     */
    private final ArrayList<BaseIssue> issueList;
    
    /**
     * Хеш соответствия проблем элементам
     */
    private final Map<Integer, ArrayList<BaseIssue>> issueHash;
    
    /**
     * Хеш соответствия проблем их идентификаторам
     */
    private final Map<Integer, BaseIssue> issueIdHash;
    
    
}
