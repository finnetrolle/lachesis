package Cores;

import Elements.BaseElement;
import java.util.Map;
import java.util.HashMap;

/**
 * Класс, реализующий функционал ядра для элементов. 
 * Имеет доступ как к корневому элементу для прохода по дереву, 
 * так и к любому элементу по его идентификатору
 * @author finnetrolle
 */
public class ElementCore {

    /**
     * Конструктор класса
     */
    public ElementCore() {
        elementHash = new HashMap<>();
        elementRoot = null;
    }
    
    /**
     * Метод возвращает элемент по его идентификатору
     * @param elementId - идентификатор элемента
     * @return ссылка на элемент
     */
    public BaseElement getElement(Integer elementId) {
        if (elementHash.containsKey(elementId)) {
            return elementHash.get(elementId);
        }
        return null;
    }
    
    /**
     * Метод добавляет элемент в хеш
     * @param element ссылка на элемент
     */
    public void addElement(BaseElement element) {
        elementHash.put(element.getId(), element);
    }
    
    /**
     * Метод возвращает корневой элемент дерева
     * @return ссылка на элемент
     */
    public BaseElement getElementRoot() {
        return elementRoot;
    }
    
    /**
     * Метод считывает дерево элементов из файла
     * @param filename 
     */
    public void readElementsFromFile(String filename) {
        elementRoot = CoreXMLReader.fillTreeWithXML(filename);
        // now fill hash
        fillHash(elementRoot);
    }
    
    /**
     * Метод заполняет хеш по дереву элементов
     * @param root 
     */
    private void fillHash(BaseElement root) {
        addElement(root);
        int count = root.getChildCount();
        for(int i = 0; i < count; ++i) {
            fillHash(root.getChild(i));
        }
    }
    
    /**
     * хеш элементов
     */
    private final Map<Integer, BaseElement> elementHash;
    
    /**
     * корневой элемент
     */
    private BaseElement elementRoot;
    
    
}
