package Cores;

import DrawingHints.BaseDrawingHint;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс, реализующий функционал ядра для графических подсказок
 * @author finnetrolle
 */
public class DrawingHintCore {

    /**
     * Конструктор класса
     */
    public DrawingHintCore() {
        items = new HashMap<>();
    }

    /**
     * Метод добавляет графическую подсказку
     * @param elementId - идентификатор элемента, к которому относится подсказка
     * @param hint - ссылка на графическую подсказку
     */
    public void addDrawingHint(Integer elementId, BaseDrawingHint hint) {
        // if we already have hint for that element - delete that hint
        if (items.containsKey(elementId)) {
            items.remove(elementId);
        }
        // add new hint
        items.put(elementId, hint);
    }
    
    /**
     * Метод возвращает графическую подсказку по идентификатору элемента
     * @param elementId - идентификатор элемента
     * @return гссылка на графическую подсказку
     */
    public BaseDrawingHint getDrawingHint(Integer elementId) {
        if (items.containsKey(elementId)) {
            return items.get(elementId);
        }
        else {
            return null;
        }
    }
    
    /**
     * Метод удаляет графическую подсказку, если она существует
     * @param elementId - идентификатор элемента
     */
    public void removeDrawingHint(Integer elementId) {
        if (items.containsKey(elementId)) {
            items.remove(elementId);
        }
    }
    
    /**
     * Метод считывает графические подсказки из XML файла
     * @param fileName - имя XML файла, содержащего подсказки
     */
    public void readDrawingHintsFromFile(String fileName) {
        CoreXMLReader.fillDrawingHintCore(this, fileName);
    }
    
    /**
     * контейнер для подсказок
     */
    private final Map<Integer, BaseDrawingHint> items;
    
}
