package Elements;

import Elements.BaseElement;

/**
 * Класс, описывающий персональный компьютер
 * @author finnetrolle
 */
public class DesktopElement extends BaseElement {
    
    /** Empty constructor creates invalid element
     * 
     */
    public DesktopElement(){
        super();
    }
    
    /** Good constructor
     * 
     * @param id - unique id of element
     * @param parent - parent of element
     */
    public DesktopElement(int id, BaseElement parent){
        super(id, parent);
    }
    
    /**
     * Переопределенный метод, возвращающий тип элемента
     * @return DESKTOP_ELEMENT
     */
    @Override
    public ElementType getElementType() {
        return ElementType.DESKTOP_ELEMENT;
    }
    
    /**
     * Переопределенный метод, возвращающий строку
     * @return 
     */
    @Override
    public String toString() {
        //return String.format("%s Desktop @ %s{%s}", super.toString(), my_hostname, my_ipAddr); // slowpoke
        super.toString();
        sb.append(" Desktop @ ");
        sb.append(String.valueOf(my_hostname));
        sb.append("{");
        sb.append(String.valueOf(my_ipAddr));
        sb.append("}");
        return sb.toString();
    }
    
    /** Method set hostname
     * 
     * @param my_hostname - hostname of Desktop
     */
    public void setHostname(String my_hostname) {
        this.my_hostname = my_hostname;
    }

    /** Method set IP Address
     * 
     * @param my_ipAddr IP Address of desktop
     */
    public void setIpAddr(String my_ipAddr) {
        this.my_ipAddr = my_ipAddr;
    }

    /** Method returns hostname
     * 
     * @return Desktop's host
     */
    public String getHostname() {
        return my_hostname;
    }

    /** Method returns IP Address
     * 
     * @return Desktop's IP Address 
     */
    public String getIpAddr() {
        return my_ipAddr;
    }

    /** hostname
     */
    private String my_hostname = "unknown";
    
    /** IP Address
     */
    private String my_ipAddr = "0.0.0.0";
   
}
