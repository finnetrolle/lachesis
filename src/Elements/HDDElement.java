package Elements;

import Elements.BaseElement;

/**
 * Класс, описывающий жесткий диск
 * @author finnetrolle
 */
public class HDDElement extends BaseElement{

    /** Constructor
     * instances invalid object
     */
    public HDDElement() {
        super();
        my_capacity = 0;
        my_freeSpace = 0;
        my_mountPoint = "";
    }
    
    /** Constructor
     * 
     * @param id - unique id
     * @param parent - parent
     */
    public HDDElement(int id, BaseElement parent){
        super(id, parent);
        my_capacity = 0;
        my_freeSpace = 0;
        my_mountPoint = "";
    }

    /** Full Constructor
     * 
     * @param my_capacity - HDD capacity
     * @param my_freeSpace - free space
     * @param my_mountPoint - mount point
     * @param id - unique id
     * @param parent - parent
     */
    public HDDElement(int my_capacity, int my_freeSpace, String my_mountPoint, int id, BaseElement parent) {
        super(id, parent);
        this.my_capacity = my_capacity;
        this.my_freeSpace = my_freeSpace;
        this.my_mountPoint = my_mountPoint;
    }

    @Override
    public ElementType getElementType() {
        return ElementType.HDD_ELEMENT;
    }

    
    
    /** Get HDD capacity
     * 
     * @return HDD Capacity
     */
    public int getCapacity() {
        return my_capacity;
    }

    /** Get free space size
     * 
     * @return free space
     */
    public int getFreeSpace() {
        return my_freeSpace;
    }

    /** Get mount point path
     * 
     * @return mount point path
     */
    public String getMountPoint() {
        return my_mountPoint;
    }

    /** set capacity value
     * 
     * @param my_capacity - HDD Capacity
     */
    public void setCapacity(int my_capacity) {
        this.my_capacity = my_capacity;
    }

    /** set free space value
     * 
     * @param my_freeSpace - free space value
     */
    public void setFreeSpace(int my_freeSpace) {
        this.my_freeSpace = my_freeSpace;
    }

    /** set mount point path
     * 
     * @param my_mountPoint path to mount point
     */
    public void setMountPoint(String my_mountPoint) {
        this.my_mountPoint = my_mountPoint;
    }

    
    /** overrided toString function
     * 
     * @return string
     */
    @Override
    public String toString() {
        //return String.format("%s HDD (%d/%d) @ '%s'", super.toString(), my_freeSpace, my_capacity, my_mountPoint); //slowpoke
        super.toString();
        sb.append(" HDD (");
        sb.append(String.valueOf(my_freeSpace));
        sb.append("/");
        sb.append(String.valueOf(my_capacity));
        sb.append(") @ '");
        sb.append(String.valueOf(my_mountPoint));
        sb.append("'");
        return sb.toString();
    }

    /** HDD capacity 
     */
    private int my_capacity;
    
    /** HDD free space
     */
    private int my_freeSpace;
    
    /** HDD mounting point
     */
    private String my_mountPoint;
}
