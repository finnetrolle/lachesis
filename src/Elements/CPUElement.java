package Elements;

import Elements.BaseElement;
import java.util.ArrayList;

/**
 * Класс описывает конкретный тип элемента - процессор
 * @author finnetrolle
 */
public class CPUElement extends BaseElement{

    /**
     * Конструктор по умолчанию.
     */
    public CPUElement() {
        super();
        coreLoads = new ArrayList<>();
    }
    
    /**
     * Конструктор рекомендуемый.
     * @param id - идентификатор элемента
     * @param parent - родительский элемент
     */
    public CPUElement(int id, BaseElement parent){
        super(id, parent);
        coreLoads = new ArrayList<>();
    }

    /**
     * Метод возвращает тип процессора
     * @return 
     */
    public String getCpuType() {
        return cpuType;
    }

    /**
     * Метод устанавливает тип процессора
     * @param cpuType - строка, содержащая наименование типа процессора
     */
    public void setCpuType(String cpuType) {
        this.cpuType = cpuType;
    }
    
    /**
     * Метод устанавливает коэффициент загрузки ядра процессора. Если ядро 
     * не зарегистрировано, для него будет создана запись. Таким образом, 
     * рекомендуется пользоваться этим методом и для регистрации ядер
     * @param coreIndex - индекс ядра
     * @param load - уровень загружки
     */
    public void setCoreLoad(int coreIndex, int load){
        // check id and length
        if (coreIndex >= coreLoads.size()){
            //create additional cores
            for (int i = coreLoads.size(); i < coreIndex; ++i){
                coreLoads.add(0);
            }
            coreLoads.add(load);
            return;
        }
        coreLoads.set(coreIndex, load);
    }

    /**
     * Перегрузка метода toString
     * @return строка, описывающая процессор
     */
    @Override
    public String toString() {
        super.toString();
        sb.append(" CPU ");
        sb.append(cpuType);
        sb.append(", cores: ");
        sb.append(getCoreCount());
        return sb.toString();
    }

    /**
     * Перегрузка метода, возвращающего тип элемента
     * @return CPU_ELEMENT
     */
    @Override
    public ElementType getElementType() {
        return ElementType.CPU_ELEMENT;
    }

    /**
     * Метод возвращает количество ядер
     * @return количество ядер
     */
    public int getCoreCount(){
        return coreLoads.size();
    }
    
    /**
     * Метод возвращает загрузку для конкретного ядра
     * @param coreIndex - индекс ядра
     * @return уровень загрузки
     */
    public int getCoreLoad(int coreIndex){
        return coreLoads.get(coreIndex);
    }
    
    /**
     * лист ядер
     */
    private final ArrayList<Integer> coreLoads;
    
    /**
     * тип процессора
     */
    private String cpuType = "unknown";
}
