/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Elements;

import java.util.ArrayList;

/**
 *
 * @author finnetrolle
 */
public class BaseElement {
    
    /**
     * Типы элементов
     */
    enum ElementType {
        BASE_ELEMENT,       // Базовый элемент
        PLACE_ELEMENT,      // Элемент место - географическое место
        BUILDING_ELEMENT,   // Элемент здание
        STAGE_ELEMENT,      // Элемент этаж
        ROOM_ELEMENT,       // Элемент комната
        WORKPLACE_ELEMENT,  // Элемент рабочее место
        DESKTOP_ELEMENT,    // Элемент персональный компьютер
        SERVER_ELEMENT,     // Элемент сервер
        THIN_CLIENT_ELEMENT,// Элемент тонкий клиент
        HDD_ELEMENT,        // Элемент жесткий диск
        CPU_ELEMENT,        // Элемент процессор
        AUDIO_ELEMENT,      // Элемент аудиокарта
        VIDEO_ELEMENT,      // Элемент видеокарта
        HUB_ELEMENT         // Элемент хаб/свитч/коммутатор/роутер
    }
    
    /** Invalid id value
     */
    public final static int invalidId;
    
    // static init
    static { 
        sb = new StringBuilder();
        invalidId = 0;
    }
    
    /**
     * Метод возвращает список идентификаторов всех родительских элементов
     * @return ArrayList<Integer> идентификаторов элементов
     */
    public ArrayList<Integer> getAllParentsIds() {
        ArrayList<Integer> list = new ArrayList<>();
        this.lookUpForParents(list);
        return list;
    }
    
    /**
     * Приватный метод для рекурсивной реализации getAllParentsIds
     * @param list - ArrayList<Integer> куда будут складываться идентификаторы
     */
    private void lookUpForParents(ArrayList<Integer> list) {
        if (this.getParent() != null) {
            list.add(this.getParent().getId());
            this.getParent().lookUpForParents(list);
        }
    }
    
    /**
     * Метод возвращает список идентификаторов всех дочерних элементов
     * @return ArrayList<Integer> идентификаторов элементов
     */
    public ArrayList<Integer> getAllChildsIds() {
        ArrayList<Integer> list = new ArrayList<>();
        this.lookDownForChilds(list);
        return list;
    }
    
    /**
     * Приватный метод для рекурсивной реализации getAllChildsIds
     * @param list - ArrayList<Integer> куда будут складываться идентификаторы
     */
    private void lookDownForChilds(ArrayList<Integer> list) {
        int count = this.getChildCount();
        for (int i = 0; i < count; ++i) {
            list.add(this.getChild(i).getId());
            this.getChild(i).lookDownForChilds(list);
        }
    }
    
    
    
    /** Redefined for every subclass
     * Метод возвращает тип элемента. Обязательно должен быть переопределен
     * в наследующих классах для корректного определения типа элемента
     * @return type of element
     */
    public ElementType getElementType(){
        return ElementType.BASE_ELEMENT;
    }
    
    /** Constructor
     * create invalid element
     */
    public BaseElement(){
        parent = null;
        id = invalidId;
        childs = new ArrayList<>();
    }
    
    /** Constructor
     * for normal instance
     * @param Id - unique id
     * @param Parent - parent in tree
     */
    public BaseElement(int Id, BaseElement Parent){
        id = Id;
        setParent(Parent);
        childs = new ArrayList<>();
    }
    
    // get methods
    /** get element's id
     * @return element's id
     */
    final public int getId(){
        return id;
    }
    
    /** get element's parent
     * 
     * @return parent
     */
    final public BaseElement getParent(){
        return parent;
    }
    
    /** get childs count
     * 
     * @return count of childs
     */
    final public int getChildCount(){
        return childs.size();
    }
    
    /** get child by id
     * 
     * @param index - unique id of child
     * @return child or null if not found
     */
    final public BaseElement getChild(int index){
        return childs.get(index);
    }
    
   
    /** method returns size of subtree with root = current element
     * 
     * @return count of all leaves in subtree with root = current element
     */
    final public int getRSize(){
        int sum = 1; // self
        for (int i=0; i < childs.size(); ++i){
            sum += childs.get(i).getRSize();
        }
        return sum;
    }
    
    // set methods
    
    /** Method removes child from vector
     * 
     * @param child - link to child
     */
    final public void removeChild(BaseElement child){
        childs.remove(getChildIndex(child));
    }
    
    /** Method return index of child in vector
     * 
     * @param child - link to child
     * @return index of child (NOT ID!!!)
     */
    final public int getChildIndex(BaseElement child){
        return childs.indexOf(child);
    }
    
    /** method add child into hash
     * 
     * @param child - link to child
     */
    final public void addChild(BaseElement child){
        childs.add(child);
    }
    
    /** method set child's parent
     * also put child into parent's hash and remove self from old parent's hash
     * @param Parent - link to new parent
     */
    final public void setParent(BaseElement Parent){
        // remove link to me from old parent
        if (parent != null){
            parent.removeChild(this);
        }
        // connect to new parent
        parent = Parent;
        if (parent != null){
            parent.addChild(this);
        }
        // done.
    }
    
    /** method releases leaf and all subleaves recoursively
     * 
     * @return count of released elements
     */
    final public int release(){
        int sum = 1;
        for (int i = 0; i < childs.size(); ++i){
            sum += childs.get(i).release();
        }
        childs.clear();
        if (parent != null){
            parent.removeChild(this);
            parent = null;
        }
        return sum;
    }

    /** Method returns basic string for element
     * <p>Clean SB tool first, then add basic substring with [id]
     * Переопределение в субклассах может быть следующего типа</p>
     * {@code super.toString();
     * sb.append("something");    
     * return sb.toString();}
     * @return basic string [id]
     */
    @Override
    public String toString() {
        //return String.format("[%d]", id()); // slowpoke
        sb.setLength(0);
        sb.append("[");
        sb.append(String.valueOf(getId()));
        sb.append("]");
        return sb.toString();
    }
    
    /** link to parent
     */
    private BaseElement parent = null;
    
    /** Static stringbuilder 
     *  Main feature is to form string for toString methods
     */
    protected static StringBuilder sb;
    
       
    /** childs
     * 
     */
    private final ArrayList<BaseElement> childs;
    
    /** unique id 
     */
    private int id = invalidId;
    
}
