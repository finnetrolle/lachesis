package Elements;

import Elements.BaseElement;

/**
 * Класс определяет элемент комнату
 * @author finnetrolle
 */
public class RoomElement extends BaseElement{

    /**
     * Конструктор
     */
    public RoomElement() {
        super();
    }
    
    /**
     * Рекомендуемый конструктор
     * @param Id - уникальный идентификатор элемента
     * @param Parent - ссылка на родительский элемент
     */
    public RoomElement(int Id, BaseElement Parent){
        super(Id, Parent);
    }
    
    /**
     * Метод возвращает имя комнаты
     * @return имя комнаты
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * Метод устанавливает имя комнаты
     * @param roomName имя комнаты
     */
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    /** Метод выводит строку по комнате
     * 
     * @return строка, описывающая комнату
     */
    @Override
    public String toString() {
        super.toString();
        sb.append(" room ");
        sb.append(roomName);
        return sb.toString();
    }

    /**
     * имя комнаты (адрес)
     */
    private String roomName = "unknown room";
}
