/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Elements;

/**
 *
 * @author finnetrolle
 */
public class WorkplaceElement extends BaseElement {

    public WorkplaceElement() {
        super();
    }

    public WorkplaceElement(String workplaceName, int Id, BaseElement Parent) {
        super(Id, Parent);
        this.workplaceName = workplaceName;
    }

    public String getWorkplaceName() {
        return workplaceName;
    }

    public void setWorkplaceName(String workplaceName) {
        this.workplaceName = workplaceName;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    private String workplaceName;
    
}
