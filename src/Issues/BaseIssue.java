package Issues;

import java.util.*;

/**
 * Класс описывает базовую проблему
 * @author finnetrolle
 */
public class BaseIssue {
    
    /** 
     * Тип (уровень) проблемы
     */
    public enum IssueLevel {
        I_INVALIDISSUE, // Инвалидная проблема (вообще не проблема)
        I_NOTICE,       // Уведомление
        I_WARNING,      // Предупреждение
        I_MINOR,        // Мелкая неисправность
        I_MAJOR,        // Серьезный неисправность
        I_CRITICAL,     // Критическая неисправность
        I_DEADLY        // Элемент сдох или вот-вот сдохнет
    }
    
    /**
     * Конструктор. По умолчанию устанавливает текущее время и задает 
     * идентификатор проблемы, основываясь на статическом счетчике
     */
    public BaseIssue(){
        id = ++idCounter;
        ownerId = 0; // all unknown issues is system's by default
        appearTime = new Date(System.currentTimeMillis());
    }

    /**
     * Метод возвращает время регистрации проблемы.
     * @return время регистрации
     */
    public Date getAppearTime() {
        return appearTime;
    }

    /**
     * Метод возвращает идентификатор проблемы
     * @return идентификатор проблемы
     */
    public int getId() {
        return id;
    }

    /**
     * Метод возвращает идентификатор элемента, к которому относится проблема
     * @return идентификатор элемента
     */
    public int getOwnerId() {
        return ownerId;
    }

    /**
     * Метод устанавливает идентификатор элемента, к которому относится проблема
     * @param ownerId - идентификатор элемента
     */
    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Метод возвращает уровень (тип) проблемы
     * @return IssueLevel тип проблемы
     */
    public IssueLevel getLevel() {
        return level;
    }

    /**
     * Метод устанавливает уровень (тип) проблемы
     * @param level - тип проблемы
     */
    public void setLevel(IssueLevel level) {
        this.level = level;
    }

    /**
     * Перегруженный метод toString
     * @return issue XXX, где XXX - идентификатор проблемы
     */
    @Override
    public String toString() {
        return "issue " + String.valueOf(id);
    }

    /**
     * Уровень (тип) проблемы
     */
    private IssueLevel level = IssueLevel.I_NOTICE;
    
    /**
     * Идентификатор проблемы
     */
    private int id = 0;
    
    /**
     * Идентификатор обладателя проблемы (элемента)
     */
    private int ownerId = 0;
    
    /**
     * Время регистрации проблемы
     */
    private Date appearTime = new Date(System.currentTimeMillis());
    
    /**
     * Статический счетчик идентификаторов для проблем
     */
    private static int idCounter = 0;
    
}
