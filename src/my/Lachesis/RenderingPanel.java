package my.Lachesis;

import GraphicsRenderer.Renderer;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * JPanel, расширенная для внедрения в нее рендерера
 * @author finnetrolle
 */
public class RenderingPanel extends JPanel{

    /**
     * Конструктор
     */
    public RenderingPanel() {
        super();
    }

    /**
     * Переопределенный метод, отвечащюий за отрисовку всего что внутри панели
     * @param g - графический контекст
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        if (renderer != null) {
            renderer.drawDefaultScene(g);
        }
    }

    /**
     * Метод устанавливает рендерер
     * @param renderer - ссылка на рендерер
     */
    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }
    
    /** 
     * Рендерер
     */
    private Renderer renderer = null;
}
