package my.Lachesis;

import Elements.BaseElement;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Класс переопределяет TreeModel для отображения дерева элементов
 * в компоненте swing.tree
 * @author finnetrolle
 */
public class BaseElementTreeModel implements TreeModel{

    /**
     * Конструктор 
     * @param root - ссылка на корневой элемент дерева BaseElement
     */
    public BaseElementTreeModel(BaseElement root){
        my_root = root;
    }

    /**
     * Конструктор по умолчанию. Требует определения корня дерева вручную
     */
    public BaseElementTreeModel() {
        my_root = null;
    }
    
    /**
     * Метод устанавливает корень дерева элементов.
     * @param root - ссылка на корневой элемент дерева BaseElement
     */
    public void setRoot(BaseElement root){
        my_root = root;
    }

    /**
     * Перегрузка метода добавления листенера
     * @param l - ссылка на листенер
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
    }

    /**
     * Перегрузка метода возврата дочернего элемента
     * @param parent - ссылка на родительский элемент
     * @param index - индекс дочернего элемента
     * @return ссылка на дочерний элемент
     */
    @Override
    public Object getChild(Object parent, int index) {
        return ((BaseElement)parent).getChild(index);
    }

    /**
     * Метод возвращает количество дочерних элементов
     * @param parent - ссылка на родительский элемент
     * @return количество дочерних элементов
     */
    @Override
    public int getChildCount(Object parent) {
        return ((BaseElement)parent).getChildCount();
    }

    /**
     * Метод возвращает индекс дочернего элемента
     * @param parent - ссылка на родительский элемент
     * @param child - ссылка на дочерний элемент
     * @return индекс дочернего элемента
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return ((BaseElement)parent).getChildIndex((BaseElement)child);
    }

    /**
     * Метод возвращает ссылку на корневой элемент дерева
     * @return ссылка на корневой элемент дерева
     */
    @Override
    public Object getRoot() {
        return my_root;
    }

    /**
     * Метод определяет является ли узел листовым
     * @param node - ссылка на узел
     * @return true если узел листовой, false - если не листовой
     */
    @Override
    public boolean isLeaf(Object node) {
        if (((BaseElement)node).getChildCount() == 0)
            return true;
        return false;
    }

    /**
     * Метод удаляет листенера (заглушка)
     * @param l - листенер
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
    }

    /**
     * Непонятно зачем и что делает. Надо дореализовать.
     * @param path
     * @param newValue 
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

        
    /**
     * root of presenting tree
     */
    private BaseElement my_root;
    
}
