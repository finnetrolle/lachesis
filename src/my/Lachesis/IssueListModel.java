/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.Lachesis;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import Interface.IssuePanel;
import Cores.IssueCore;
import Issues.BaseIssue;

/**
 *
 * @author finnetrolle
 */
public class IssueListModel implements ListModel {

    public IssueListModel(IssueCore issueCore) {
        panels = new ArrayList<>();
        iCore = issueCore;
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        
    }

    @Override
    public Object getElementAt(int index) {
        return ((IssuePanel)panels.get(index));
    }

    @Override
    public int getSize() {
        return panels.size();
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        
    }
    
    public void updateModel() {
        panels.clear();
        if (iCore == null) {
            return;
        }
        ArrayList<BaseIssue> list;
        if (currentElementId == null) {
            list = iCore.getIssues();
        } 
        else {
            list = iCore.getIssuesForElement(currentElementId);
        }
        if (list == null) {
            return;
        }
        // now we have a list. refill issuepanel list
        for (int i = 0; i < list.size(); ++i) {
            IssuePanel panel = new IssuePanel();
            panel.setIssueCaption("Заголовок");
            BaseIssue issue = list.get(i);
            panel.setElementId(issue.getOwnerId());
            panel.setIssueId(issue.getId());
            panel.setIssueLevel(issue.getLevel());
            panels.add(panel);
        }
    }
    
    public void setCurrentElementId(int elementId) {
        currentElementId = elementId;
    }
    
    public void resetCurrentElementId() {
        currentElementId = null;
    }
    
    public void setIssueCore(IssueCore iCore) {
        this.iCore = iCore;
    }
    
    private ArrayList<IssuePanel> panels = null;
    private IssueCore iCore = null;
    private Integer currentElementId = null;
    
}
