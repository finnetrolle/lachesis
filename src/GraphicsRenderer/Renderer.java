package GraphicsRenderer;

import Cores.DrawingHintCore;
import Cores.ElementCore;
import DrawingHints.BaseDrawingHint;
import Issues.BaseIssue;
import Elements.BaseElement;
import java.awt.Graphics;

/**
 * Класс, выполняющий рендеринг сцены
 * @author finnetrolle
 */
public class Renderer {

    /**
     * Конструктор. Чтобы был.
     */
    public Renderer() {
    }

    /** 
     * Рекомендуемый конструктор
     * @param eCore - ссылка на ядро элементов
     * @param dhCore - ссылка на ядро подсказок
     */
    public Renderer(ElementCore eCore, DrawingHintCore dhCore) {
        this.elementCore = eCore;
        this.drawingHintCore = dhCore;
    }
    
    /**
     * Метод задает ядро элементов
     * @param elementCore - ссылка на яжро элементов
     */
    public void setElementCore(ElementCore elementCore) {
        this.elementCore = elementCore;
    }

    /**
     * Метод задает ядро подсказок
     * @param drawingHintCore ссылка на ядро подсказок
     */
    public void setDrawingHintCore(DrawingHintCore drawingHintCore) {
        this.drawingHintCore = drawingHintCore;
    }
    
    /**
     * Метод, отрисовывающий сцену по умолчанию, без учета проблем
     * @param g - графический контекст
     */
    public void drawDefaultScene(Graphics g) {
        drawDefaultScene(g, elementCore.getElementRoot());
    }
    
    /**
     * Рекурсивный метод, рисующий сцену по умолчанию
     * @param g - графический контекст
     * @param root - текущий элемент
     */
    private void drawDefaultScene(Graphics g, BaseElement root) {
        BaseDrawingHint hint = drawingHintCore.getDrawingHint(root.getId());
        if (hint != null) {
            hint.draw(g, BaseIssue.IssueLevel.I_INVALIDISSUE);
            g.translate(hint.getX(), hint.getY());
        }
        
        for (int i = 0; i < root.getChildCount(); ++i) {
            drawDefaultScene(g, root.getChild(i));
        }
        if (hint != null) {
            g.translate(-hint.getX(), -hint.getY());
        }
        
    }
    
    /**
     * ссылка на ядро элементов
     */
    private ElementCore elementCore = null;
    
    /**
     * ссылка на ядро подсказок
     */
    private DrawingHintCore drawingHintCore = null;

    
}
