/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Interface;

import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import Interface.IssuePanel;

/**
 *
 * @author finnetrolle
 */
public class IssueCellRenderer implements ListCellRenderer{

    public IssueCellRenderer() {
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        IssuePanel panel = ((IssuePanel)value);
        panel.setSize(list.getWidth(), 25);
        return panel;
    }
    
    
    
    

}
