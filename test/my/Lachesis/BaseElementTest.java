/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.Lachesis;

import Elements.BaseElement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author finnetrolle
 */
public class BaseElementTest {
    
    public BaseElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of id method, of class BaseElement.
     */
    @Test
    public void testId() {
        System.out.println("id");
        BaseElement instance = null;
        instance = new BaseElement();
        assertEquals(0, instance.getId());
        instance = new BaseElement(1, null);
        assertEquals(1, instance.getId());
        instance = new BaseElement(154, null);
        assertEquals(154, instance.getId());
        instance = new BaseElement(200, null);
        assertEquals(200, instance.getId());
    }

    /**
     * Test of parent method, of class BaseElement.
     */
    @Test
    public void testParent() {
        System.out.println("parent");
        BaseElement instance;
        BaseElement instance2;
        BaseElement root;
        root = new BaseElement();
        assertEquals(null, root.getParent());
        instance = new BaseElement(1, root);
        assertEquals(root, instance.getParent());
        instance2 = new BaseElement(2, root.getChild(0));
        assertEquals(instance, instance2.getParent());
    }

    /**
     * Test of childCount method, of class BaseElement.
     */
    @Test
    public void testChildCount() {
        System.out.println("childCount");
        BaseElement instance = new BaseElement();
        BaseElement child;
        int expResult = 5;
        for (int i = 0; i < 5; ++i)
            child = new BaseElement(i, instance);
        int result = instance.getChildCount();
        assertEquals(expResult, result);
    }

    /**
     * Test of child method, of class BaseElement.
     */
    @Test
    public void testChild() {
        System.out.println("child");
        int index = 0;
        BaseElement instance = new BaseElement();
        BaseElement child;
        child = new BaseElement(1, instance);
        BaseElement result = instance.getChild(0);
        assertEquals(child, result);
    }

    /**
     * Test of rSize method, of class BaseElement.
     */
    @Test
    public void testRSize() {
        System.out.println("rSize");
        BaseElement instance = new BaseElement();
        int expResult = 1;
        int result = instance.getRSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of removeChild method, of class BaseElement.
     */
    @Test
    public void testRemoveChild() {
        System.out.println("removeChild");
        BaseElement child = null;
        BaseElement instance = new BaseElement();
        child = new BaseElement(1, instance);
        instance.removeChild(child);
        assertEquals(instance.getChildCount(), 0);
    }

    /**
     * Test of childIndex method, of class BaseElement.
     */
    @Test
    public void testChildIndex() {
        System.out.println("childIndex");
        BaseElement child1 = null;
        BaseElement child2 = null;
        BaseElement child3 = null;
        BaseElement instance = new BaseElement();
        child1 = new BaseElement(1, instance);
        child2 = new BaseElement(2, instance);
        child3 = new BaseElement(3, instance);
        assertEquals(0, instance.getChildIndex(child1));
        assertEquals(1, instance.getChildIndex(child2));
        assertEquals(2, instance.getChildIndex(child3));
        assertEquals(-1, instance.getChildIndex(null));
    }

    /**
     * Test of addChild method, of class BaseElement.
     */
    @Test
    public void testAddChild() {
        System.out.println("addChild");
        BaseElement child = null;
        BaseElement instance = new BaseElement();
        instance.addChild(child);
        assertEquals(instance.getChild(0), child);
    }

    /**
     * Test of setParent method, of class BaseElement.
     */
    @Test
    public void testSetParent() {
        System.out.println("setParent");
        BaseElement firstParent = new BaseElement(0, null);
        BaseElement secondParent = new BaseElement(1, null);
        BaseElement son = new BaseElement(2, firstParent);
        BaseElement grandSon = new BaseElement(3, son);
        son.setParent(secondParent);
        assertEquals(son.getParent(), secondParent);
        assertEquals(grandSon.getParent().getParent(), secondParent);
        assertEquals(firstParent.getRSize(), 1);
        assertEquals(secondParent.getRSize(), 3);
        grandSon.setParent(firstParent);
        assertEquals(firstParent, grandSon.getParent());
        assertEquals(firstParent.getRSize(), 2);
        assertEquals(secondParent.getRSize(), 2);
    }

    /**
     * Test of release method, of class BaseElement.
     */
    @Test
    public void testRelease() {
        System.out.println("release");
        BaseElement root = new BaseElement();
        BaseElement child = new BaseElement(1, root);
        child = new BaseElement(2, root.getChild(0));
        child.getParent().release();
        assertEquals(root.getChildCount(), 0);
    }

    /**
     * Test of toString method, of class BaseElement.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        BaseElement instance = new BaseElement(435, null);
        String expResult = "[435]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
