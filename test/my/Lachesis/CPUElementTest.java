/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.Lachesis;

import Elements.CPUElement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author finnetrolle
 */
public class CPUElementTest {
    
    public CPUElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getCpuType method, of class CPUElement.
     */
    @Test
    public void testGetCpuType() {
        System.out.println("getCpuType");
        CPUElement instance = new CPUElement();
        assertEquals("unknown", instance.getCpuType());
        String s = "Intel Core i7 4470";
        instance.setCpuType(s);
        assertEquals(s, instance.getCpuType());
    }

    /**
     * Test of setCpuType method, of class CPUElement.
     */
    @Test
    public void testSetCpuType() {
        System.out.println("setCpuType");
        CPUElement instance = new CPUElement();
        String s = "Intel Core i7 4470";
        instance.setCpuType(s);
        assertEquals(s, instance.getCpuType());
    }

    /**
     * Test of setCoreLoad method, of class CPUElement.
     * Also testing getCoreLoad() and getCoreCount()
     */
    @Test
    public void testSetCoreLoad() {
        System.out.println("setCoreLoad");
        int coreIndex = 0;
        int load = 0;
        CPUElement instance = new CPUElement();
        instance.setCoreLoad(coreIndex, load);
        assertEquals(instance.getCoreCount(), 1);
        instance.setCoreLoad(10, 10);
        assertEquals(instance.getCoreCount(), 11);
        assertEquals(instance.getCoreLoad(10), 10);
    }

    
}
